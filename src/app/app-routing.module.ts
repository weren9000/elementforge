import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardUserComponent } from './board-user/board-user.component';
import { BoardModeratorComponent } from './board-moderator/board-moderator.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';
import { StartPageComponent } from './start-page/start-page.component';
import { ListCharacterComponent } from './list-character/list-character.component';
import { NewCharacterComponent } from './new-character/new-character.component';
import { DeleteCharacterComponent } from './delete-character/delete-character.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CharacterDetailComponent } from './character-detail/character-detail.component';
import { WalletComponent } from './wallet/wallet.component';
import { TransferComponent } from './transfer/transfer.component';

const routes: Routes = [
  { path: 'character', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'user', component: BoardUserComponent },
  { path: 'mod', component: BoardModeratorComponent },
  { path: 'admin', component: BoardAdminComponent },
  { path: 'start', component: StartPageComponent },
  { path: 'list-сharacter', component: ListCharacterComponent },
  { path: 'new-character', component: NewCharacterComponent },
  { path: 'delete-character', component: DeleteCharacterComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'character/:id', component: CharacterDetailComponent },
  { path: 'transfer', component: TransferComponent },
  { path: 'wallet', component: WalletComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }