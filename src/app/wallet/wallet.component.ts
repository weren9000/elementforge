import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from './../_services/token-storage.service';
import { WALLET } from '../_mock/character-mock';
@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.css']
})
export class WalletComponent implements OnInit {
  public wallets: any = WALLET;
  public total: any;
  public currentUser: string = 'Гость';
  constructor(private tokenStorageService: TokenStorageService,) { }

  ngOnInit(): void {
    this.total = 3000150.02
      const user = this.tokenStorageService.getUser();
      this.currentUser = user.replace(/['"]+/g, '').split('@')[0];
      console.log(this.currentUser);
      

  }

}
