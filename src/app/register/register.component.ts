import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: any = {
    email: null,
    password: null,
    first_name: null,
    last_name: null,
    patronymic: null,
    phone: null,
    vk_id: null,
    date_of_birth: null,
    agreement: null,
  };
  
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(
    private authService: AuthService, 
    private router: Router,
    ) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    const { email, password, first_name, last_name, patronymic, phone,vk_id, date_of_birth, agreement} = this.form;

    this.authService.register(email, password, first_name, last_name, patronymic, phone,vk_id, date_of_birth, agreement).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }

  public onSelectMenu(select: any): void {
    this.router.navigate([select]);
  }

}