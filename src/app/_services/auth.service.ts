import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const AUTH_API = 'http://188.17.152.92:8080/api/';
// const AUTH_API = 'http://192.168.0.57:80/api/';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'signin', {
      email,
      password
    }, httpOptions);
  }

  register(email: string, password: string, first_name: string, last_name: string, patronymic: string, phone: string, vk_id: string, date_of_birth: Date, agreement:  boolean): Observable<any> {
    return this.http.post(AUTH_API + 'signup', {
      email, 
      password, 
      first_name,
      last_name, 
      patronymic, 
      phone,
      vk_id, 
      date_of_birth,
      agreement
    }, httpOptions);
  }
}