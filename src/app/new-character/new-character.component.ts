import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-new-character',
  templateUrl: './new-character.component.html',
  styleUrls: ['./new-character.component.css']
})
export class NewCharacterComponent implements OnInit {
  races: string[] = [
    'Имперец',
    'Альдмер',
    'Норд',
    'Бретонец',
    'Босмер',
    'Орксимер',
    'Каджит',
    'Редгард', 
    'Данимер',
    'Норд',
    'Аргонианин',
    'Даэдра'
  ];

  capabilities: string[] = [
    '-',
    'Алхимик',
    'Зачарователь',
    'Кузнец',
    'Тактик',
    'Полководец',
    'Экономист',
    'Гильд-мастер',
    'Касание огня',
    'Касание мороза',
    'Касание шоком',
    'Паралич',
    'Даэдрический язык',
    'Драконий язык',
    'Аутлейтский язык',
    'Двемерский язык'
  ]

  states: string[] = [
    '-',
    'Империя',
    'Острова Саммерсет',
    'Хай Рок',
    'Скайрим',
    'Валенвуд',
    'Эльсвеер',
    'Морровинд',
    'Чернотопье',
    'Обливион',
    'Не знаю'
  ]

  isLoggedIn = false;
  myForm : FormGroup;
  constructor(){
      this.myForm = new FormGroup({      
        "role": new FormControl("", [Validators.minLength(3), Validators.required]),
        "last_name": new FormControl(""),
        "first_name": new FormControl("", Validators.required),
        "patronymic": new FormControl(""),
        "age": new FormControl("", [Validators.min(14),Validators.max(300),  Validators.required]),
        "race": new FormControl("", Validators.required),
        "gender": new FormControl("", Validators.required), 
        "born_state": new FormControl("", Validators.required), 
        "born_city": new FormControl(""), 
        "religion": new FormControl(""), 
        "patron_god": new FormControl(""),  
        "father": new FormControl(""),  
        "mother": new FormControl(""), 
        "brothers": new FormControl(""), 
        "sisters": new FormControl(""), 
        "relatives": new FormControl(""), 
        "hobbies": new FormControl(""), 
        "purpose": new FormControl(""), 
        "quenta": new FormControl(""), 
        "capabilities_1": new FormControl(""),
        "grade_1": new FormControl(""),  
        "capabilities_2": new FormControl(""), 
        "grade_2": new FormControl(""),  
        "capabilities_3": new FormControl(""), 
        "grade_3": new FormControl(""),  
        "capabilities_4": new FormControl(""),
        "grade_4": new FormControl("")  
      });
  }
    
  submit(){
      console.log(this.myForm);
  }

  clearForm(){
    this.myForm.reset();
    console.log(this.myForm);
}

  ngOnInit(){

  }

}

