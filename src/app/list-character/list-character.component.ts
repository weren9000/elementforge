import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CHARACTER } from '../_mock/character-mock';

@Component({
  selector: 'app-list-character',
  templateUrl: './list-character.component.html',
  styleUrls: ['./list-character.component.css']
})
export class ListCharacterComponent implements OnInit {
  public filterActive: boolean = false;

  public characters: any = CHARACTER;

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  public pressed(): void {
    this.filterActive = !this.filterActive;
    console.log(this.filterActive);
  }

  public onSelectMenu(select: any): void {
    this.router.navigate([select]);
  }

  public onSelectCharacter(select:any):void {
    console.log(select.id);
    select = '/character/' + select.id;
    this.router.navigate([select]);
  }

}
