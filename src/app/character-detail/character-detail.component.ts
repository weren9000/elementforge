import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CHARACTER } from '../_mock/character-mock';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.css']
})
export class CharacterDetailComponent implements OnInit {

  public characters: any = CHARACTER

  public persona: any;
  constructor(private router: Router) { }
  public USER_KEY: string = 'auth-character';
  public currentCharacter: any = '';
  ngOnInit(): void {

    console.log(this.router.url);
    this.persona = this.router.url.replace(/[^0-9]/g, '')
    console.log(this.persona);
    window.sessionStorage.setItem(this.USER_KEY, JSON.stringify(this.persona));
    this.persona = window.sessionStorage.getItem(this.USER_KEY)
    this.persona = JSON.parse(this.persona);
    console.log(this.persona);
    this.characters = this.characters[this.persona-1]
  }

}
